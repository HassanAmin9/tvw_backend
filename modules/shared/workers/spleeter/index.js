
const rabbitmqService = require('../../vendors/rabbitmq');
const queues = require('../../vendors/rabbitmq/queues');

const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;

let rabbitmqChannel;
rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
    if (err) {
        throw err;
    }
    rabbitmqChannel = channel;
});

function extractVideoBackgroundMusic(videoId) {
    return rabbitmqChannel.sendToQueue(queues.EXTRACT_VIDEO_BACKGROUND_MUSIC_QUEUE, new Buffer(JSON.stringify({ videoId })), { persistent: true });
}


module.exports = {
    extractVideoBackgroundMusic
}