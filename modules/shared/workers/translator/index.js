const rabbitmqService = require('../../vendors/rabbitmq');
const queues = require('../../vendors/rabbitmq/queues');
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;

let rabbitmqChannel;
rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
    if (err) {
        throw err;
    }
    rabbitmqChannel = channel;
})

function translateArticleText({ articleId, lang }) {
    return rabbitmqChannel.sendToQueue(queues.TRANSLATE_ARTICLE_TEXT_QUEUE, new Buffer(JSON.stringify({ articleId, lang })), { persistent: true });
}

module.exports = {
    translateArticleText,
}