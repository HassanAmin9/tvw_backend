
const rabbitmqService = require('../../vendors/rabbitmq');
const queues = require('../../vendors/rabbitmq/queues');

const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;

let rabbitmqChannel;
rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
    if (err) {
        throw err;
    }
    rabbitmqChannel = channel;
});



function transcribeVideo(videoId) {
    return rabbitmqChannel.sendToQueue(queues.TRANSCRIBE_VIDEO_QUEUE, new Buffer(JSON.stringify({ videoId })), { persistent: true });
}

function convertVideoToArticle(videoId) {
    return rabbitmqChannel.sendToQueue(queues.CONVERT_VIDEO_TO_ARTICLE_QUEUE, new Buffer(JSON.stringify({ videoId })), { persistent: true });
}

module.exports = {
    transcribeVideo,
    convertVideoToArticle,
}