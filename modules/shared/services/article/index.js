const async = require('async');
const storageService = require('../../vendors/storage');
const articleHandler = require('../../dbHandlers/article');

const BaseService = require('../BaseService');

class ArticleService extends BaseService {
    constructor() {
        super(articleHandler);
    }


    createArticle(values) {
        return this.create(values)
    }


    deleteArticlesRecordedAudios(articles) {
        const deleteKeys = [];
        const deleteFuncArray = [];
        articles.forEach((article) => {
            const articleSlides = article.slides.reduce((acc, s) => acc.concat(s.content), []);
            articleSlides.forEach((slide) => {
                // Delete audios
                if (slide.slide.audioKey) {
                    deleteKeys.push(slide.audioKey);
                }
                // Delete slides media only if it's original article and not archived
                if (article.articleType === 'original') {
                    slide.media.forEach((mitem) => {
                        if (mitem.mediaKey) {
                            deleteKeys.push(mitem.mediaKey);
                        }
                    })
                }
            })
        })
        deleteKeys.forEach((key) => {
            deleteFuncArray.push((cb) => {
                const [directoryName, fileName] = key.split('/');
                storageService.deleteFile(directoryName, fileName)
                    .then((res) => {
                        console.log(res);
                        return cb(null, res);
                    })
                    .catch((err) => {
                        console.log(err);
                        return cb();
                    })
            })
        })

        async.parallelLimit(deleteFuncArray, 5, (err, result) => {
            console.log('deleted articles media', err, articles, result)
        })
    }



    getSlideIndex(article, slidePosition, subslidePosition) {
        const slidesComments = article.slides
            .reduce((acc, s) => acc.concat(s.content.map((sub) => ({ ...sub, slidePosition: s.position }))), [])
            .filter(s => s.speakerProfile && s.speakerProfile.speakerNumber !== -1)
            .map((s, index) => ({ slidePosition: s.slidePosition, subslidePosition: s.position, index, comments: [] }));
        return slidesComments.find(s => s.slidePosition === slidePosition && s.subslidePosition === subslidePosition).index;
    }

    getArticleLanguage(article) {
        const langObj = {};
        if (article.langCode) {
            langObj.langCode = article.langCode;
        }
        if (article.langName) {
            langObj.langName = article.langName;
        }
        if (article.tts) {
            langObj.tts = true;
        }
        return langObj;
    }

    isArticleCompleted(article) {
        const slides = article.slides.reduce((acc, s) => acc.concat(s.content), []).filter((s) => s.speakerProfile && s.speakerProfile.speakerNumber !== -1);
        return slides.every(s => s.audio && s.text);
    }

    // function deleteArticlesMedia(articles) {
    //     const deleteKeys = [];
    //     const deleteFuncArray = [];
    //     articles.forEach((article) => {
    //         const articleSlides = article.slides.reduce((acc, s) => acc.concat(s.content), []);
    //         articleSlides.forEach((slide) => {
    //             // Delete audios
    //             if (slide.audioKey) {
    //                 deleteKeys.push(slide.audioKey);
    //             }
    //             // Delete slides media only if it's original article and not archived
    //             if (article.articleType === 'original') {
    //                 slide.media.forEach((mitem) => {
    //                     if (mitem.mediaKey) {
    //                         deleteKeys.push(mitem.mediaKey);
    //                     }
    //                 })   
    //             }
    //         })
    //     })
    //     deleteKeys.forEach((key) => {
    //         deleteFuncArray.push((cb) => {
    //             const [ directoryName, fileName ] = key.split('/');
    //             storageService.deleteFile(directoryName, fileName)
    //             .then((res) => {
    //                 console.log(res);
    //                 return cb(null, res);
    //             })
    //             .catch((err) => {
    //                 console.log(err);
    //                 return cb();
    //             })
    //         })
    //     })

    //     async.parallelLimit(deleteFuncArray, 5, (err, result) => {
    //         console.log('deleted articles media', err, articles, result)
    //     })
    // }

    getSlideAndSubslideIndexFromPosition(slides, slidePosition, subslidePosition) {
        const slideIndex = slides.findIndex((s) => parseInt(s.position) === parseInt(slidePosition));
        console.log(slidePosition, slideIndex)
        if (slideIndex === -1) return {};
        const subslideIndex = slides[slideIndex].content.findIndex((s) => parseInt(s.position) === parseInt(subslidePosition));
        return { slideIndex, subslideIndex };
    }

    reorderSlidesAndContent(slides) {
        slides.forEach((slide, slideIndex) => {
            slide.position = slideIndex;
            if (slide.content) {
                slide.content = slide.content.sort((a, b) => a.startTime - b.startTime);
                slide.content.forEach((subslide, subslideIndex) => {
                    subslide.position = subslideIndex;
                })
            }
        })

        return slides;
    }

    addSubslide(articleId, slidePosition, subslidePosition, subslide) {
        return new Promise((resolve, reject) => {
            this.findById(articleId)
                .then((article) => {
                    if (!article) return reject(new Error('Invalid article'));
                    const { slides } = article.toObject();
                    let { slideIndex, subslideIndex } = this.getSlideAndSubslideIndexFromPosition(slides, slidePosition, subslidePosition);
                    if (slideIndex == undefined) {
                        slideIndex = 0
                    }
                    if (subslideIndex === undefined) {
                        subslideIndex = 0;
                    }
                    console.log('adding subslide', subslide, slideIndex)
                    if (slides[slideIndex]) {
                        slides[slideIndex].content.splice(subslideIndex, 0, subslide)
                    } else {
                        slides[slideIndex] = { content: [subslide] };
                    }
                    // Re-order slide content positions
                    slides[slideIndex].content = slides[slideIndex].content.map((subslide, index) => ({ ...subslide, position: index }));

                    return this.updateById(articleId, { slides: this.reorderSlidesAndContent(slides) })
                })
                .then(resolve)
                .catch(reject)
        });
    }

    updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, changes) {
        return new Promise((resolve, reject) => {
            this.findById(articleId)
                .then((article) => {
                    if (!article) return reject(new Error('Invalid article'));
                    const { slides } = article.toObject();
                    const { slideIndex, subslideIndex } = this.getSlideAndSubslideIndexFromPosition(slides, slidePosition, subslidePosition);
                    const subslides = slides.reduce((acc, s) => s.content ? acc.concat(s.content.map((ss) => ({ ...ss, slidePosition: s.position, subslidePosition: ss.position}))) : acc, []).sort((a, b) => a.startTime - b.startTime).map((s, index) => ({ ...s, index}));
                    let update = {}
                    Object.keys(changes).forEach((key) => {
                        // slides[slideIndex].content[subslideIndex][key] = changes[key];
                        if (key === 'text') {
                            changes[key] = changes[key].split('.').map(s => s.trim()).join('. ');
                        } else if (key === 'startTime' || key === 'endTime') {
                            const itemIndex = subslides.findIndex(s => s.slidePosition === parseInt(slidePosition) && s.subslidePosition === parseInt(subslidePosition));
                            const prevItem = subslides[itemIndex - 1];
                            const nextItem = subslides[itemIndex + 1];

                            if (key === 'startTime') {
                                if (changes[key] > subslides[itemIndex].endTime) {
                                    throw new Error('Start time cannot be larger than end time');
                                }
                                console.log('prev item',prevItem, changes[key])
                                if (prevItem && changes[key] < prevItem.endTime) {
                                    changes[key] = prevItem.endTime;
                                }
                            } else if (key === 'endTime') {
                                if (changes[key] < subslides[itemIndex].startTime ) {
                                    throw new Error('End time cannot be less than start time');
                                }
                                if (nextItem && changes[key] > nextItem.startTime) {
                                    changes[key] = nextItem.startTime;
                                }
                            }
                        }
                        
                        update[`slides.${slideIndex}.content.${subslideIndex}.${key}`] = changes[key];
                    })
                    return this.updateById(article._id, { ...update });
                })
                .then(() => resolve(changes))
                .catch(reject);
        })
    }

    replaceArticleSlidesText(articleId, { find, replace } = {}) {
        return new Promise((resolve, reject) => {
            const slidesChanges = {};
            const changedSlides = [];

            this.findById(articleId)
                .then((articleDoc) => {
                    const article = articleDoc.toObject();
                    article.slides.forEach((slide, slideIndex) => {
                        slide.content.forEach((subslide, subslideIndex) => {
                            const specialChars = `\\[|\\$|\\&|\\+|\\,|\\।|\\:|\\;|\\=|\\?|\\@|\\#|\\||\\'|\\<|\\>|\\.|\\^|\\*|\\(|\\)|\\%|\\!|\\-|\\]|\\s`
                            const re = new RegExp(`(${specialChars}|^)${find}(${specialChars}|$)`, 'ig')
                            if (subslide.text && subslide.text.trim().length > 0 && subslide.text.match(re)) {
                                const newText = subslide.text.replace(re, `\$1${replace}$2`);
                                slidesChanges[`slides.${slideIndex}.content.${subslideIndex}.text`] = newText;
                                slidesChanges[`slides.${slideIndex}.content.${subslideIndex}.audioSynced`] = false;
                                changedSlides.push({
                                    slidePosition: slide.position,
                                    subslidePosition: subslide.position,
                                    text: newText
                                });
                            }
                        })
                    })
                    return this.update({ _id: articleId }, slidesChanges)
                })
                .then(() => {
                    return resolve(changedSlides)
                })
                .catch(reject);
        })
    }

    updateSubslide(articleId, slideIndex, subslideIndex, changes) {
        return new Promise((resolve, reject) => {
            this.findById(articleId)
                .then((article) => {
                    if (!article) return reject(new Error('Invalid article'));
                    const { slides } = article;
                    Object.keys(changes).forEach((key) => {
                        slides[slideIndex].content[subslideIndex][key] = changes[key];
                    })
                    return this.updateById(article._id, { slides })
                })
                .then(resolve)
                .catch(reject);
        })
    }


    splitSubslide(articleId, slidePosition, subslidePosition, wordIndex, time) {
        return new Promise((resolve, reject) => {
            this.findById(articleId)
                .then((article) => {
                    if (!article) throw new Error('Invalid article id');
                    const { slides } = article.toObject();
                    const { slideIndex, subslideIndex } = this.getSlideAndSubslideIndexFromPosition(slides, slidePosition, subslidePosition);
                    const splittedSubslide = slides[slideIndex].content[subslideIndex];
                    const subslideDuration = splittedSubslide.endTime - splittedSubslide.startTime;
                    const splitDuration = this.getDurationBeforeWord(splittedSubslide.text, subslideDuration, wordIndex);
                    let newSubslides = [
                        {
                            ...splittedSubslide,
                            text: splittedSubslide.text.split(' ').slice(0, wordIndex).join(' '),
                            startTime: splittedSubslide.startTime,
                            endTime: time,
                        },
                        {
                            ...splittedSubslide,
                            text: splittedSubslide.text.split(' ').slice(wordIndex).join(' '),
                            startTime: time,
                            endTime: splittedSubslide.endTime,
                        }
                    ];
                    newSubslides.forEach((s) => {
                        delete s._id;
                    })
                    slides[slideIndex].content.splice(subslideIndex, 1, ...newSubslides);
                    // Re-update indexes
                    slides[slideIndex].content = slides[slideIndex].content.map((subslide, index) => {
                        return { ...subslide, position: index };
                    })

                    return this.updateById(articleId, { slides: this.reorderSlidesAndContent(slides) });
                })
                .then(resolve)
                .catch(reject);
        });
    }

    removeSubslide(articleId, slidePosition, subslidePosition) {
        return new Promise((resolve, reject) => {
            this.findById(articleId)
                .then((article) => {
                    if (!article) return reject(new Error('Invalid article'));
                    const { slides } = article;
                    const { slideIndex, subslideIndex } = this.getSlideAndSubslideIndexFromPosition(slides, slidePosition, subslidePosition);
                    slides[slideIndex].content.splice(subslideIndex, 1);
                    return this.updateById(article._id, { slides: this.reorderSlidesAndContent(slides) })
                })
                .then(resolve)
                .catch(reject);
        })
    }


    formatSubslideToSubtitle(subslide) {
        return ({ ...subslide, startTime: subslide.startTime * 1000, endTime: subslide.endTime * 1000, text: subslide.text, speakerNumber: subslide.speakerProfile.speakerNumber })
    }

    formatSlidesToSubslides(slides) {
        return slides.reduce((acc, s, slideIndex) => s.content && s.content.length > 0 ? acc.concat(s.content.map((ss, subslideIndex) => ({ ...ss, slideIndex, subslideIndex, slidePosition: s.position, subslidePosition: ss.position }))) : acc, []).sort((a, b) => a.startTime - b.startTime);

    }

    getDurationBeforeWord(text, duration, wordIndex) {
        const wordTime = duration / text.split(' ').length;
        return text.split(' ').slice(0, wordIndex).length * wordTime;
    }

    cleanArticleSilentSlides(article) {
        let clonedArticle;
        if (article.toObject) {
            clonedArticle = article.toObject();
        } else {
            clonedArticle = { ...article };
        }
        clonedArticle.slides.forEach(slide => {
            slide.content = slide.content.filter((s) => !s.silent);
        });
        clonedArticle.slides = clonedArticle.slides.filter((s) => s.content.length > 0);;
        return clonedArticle;
    }

    cleanArticleBackgroundMusicSlides(article) {
        let clonedArticle;
        if (article.toObject) {
            clonedArticle = article.toObject();
        } else {
            clonedArticle = { ...article };
        }
        clonedArticle.slides.forEach(slide => {
            slide.content = slide.content.filter((s) => s.speakerProfile.speakerNumber !== -1);
        });
        clonedArticle.slides = clonedArticle.slides.filter((s) => s.content.length > 0);;
        return clonedArticle;
    }

    cleanArticleSilentAndBackgroundMusicSlides(article) {
        let clonedArticle;
        if (article.toObject) {
            clonedArticle = article.toObject();
        } else {
            clonedArticle = { ...article };
        }
        return this.cleanArticleBackgroundMusicSlides(this.cleanArticleSilentSlides(clonedArticle));
    }

    cloneArticle(articleId) {
        return new Promise((resolve, reject) => {
            this.findById(articleId)
                .then((article) => {
                    article = article.toObject();
                    article.originalArticle = article._id;
                    // article.translators = [userId];
                    delete article._id;
                    return this.create(article);
                })
                .then(resolve)
                .catch(reject)
        })
    }

    syncTranslationArticlesMediaWithOriginal(originalArticleId) {
        return new Promise((resolve, reject) => {
            let originalArticle;
            this.findById(originalArticleId)
                .then((originalArticleDoc) => {
                    if (!originalArticleDoc) throw new Error('Invalid article id');
                    originalArticle = originalArticleDoc.toObject();
                    return find({ originalArticle: originalArticleId, articleType: 'translation' })
                })
                .then((translationArticles) => {
                    if (!translationArticles || translationArticles.length === 0) return resolve();
                    const updateArticleFuncArray = [];
                    translationArticles.forEach((article) => {
                        article = article.toObject();
                        updateArticleFuncArray.push((cb) => {
                            //   Update background music slides video/audio
                            article.slides.forEach((slide) => {
                                slide.content.forEach((subslide) => {
                                    const origianlSubslide = originalArticle.slides.find(s => s.position === slide.position).content.find(s => s.position === subslide.position);
                                    // overwrite media/video data
                                    subslide.media = origianlSubslide.media;
                                    // For bacgkround music slides, overwrite audio data
                                    if (subslide.speakerProfile && subslide.speakerProfile.speakerNumber === -1) {
                                        subslide.audio = origianlSubslide.audio;
                                        subslide.audioKey = origianlSubslide.audioKey;
                                    }
                                })
                            })
                            this.update({ _id: article._id }, { slides: article.slides })
                                .then(() => {
                                    cb();
                                })
                                .catch(err => {
                                    console.log('error updating article', err);
                                    cb();
                                })
                        })
                    })
                    async.series(updateArticleFuncArray, () => {
                        console.log('done updating translate articles')
                        resolve();
                    })
                })
                .catch(reject);
        })
    }

    /*
        Validations
    */

   getOverlappedSubslide({ startTime, endTime }, slides, skipPositions = []) {
    const subslides = slides.reduce((acc, s) => s.content && s.content.length > 0 ? acc.concat(s.content.map((ss) => ({ ...ss, slidePosition: s.position, subslidePosition: ss.position }))) : acc, []);
    skipPositions = skipPositions.map(s => ({ slidePosition: parseInt(s.slidePosition), subslidePosition: parseInt(s.subslidePosition) }));
    const skipSlidePositions = skipPositions.map(s => s.slidePosition);
    const skipSubslidePositions = skipPositions.map(s => s.subslidePosition);

    return subslides.find(s => {
            // Skip positions
            console.log(s.slidePosition, s.subslidePosition);
            if (skipSlidePositions.indexOf(s.slidePosition) !== -1 && skipSubslidePositions[skipSlidePositions.indexOf(s.slidePosition)] === s.subslidePosition) {
                return false;
            }
            // exact startTime/endTime
            if (s.startTime === startTime || s.endTime === endTime) return true;
            // new subtitle is dropped in the range of another subtitle
            if (endTime < s.endTime && startTime > s.startTime) return true;
            // new subtite startTime is within another subtitle
            if (s.startTime < startTime && s.endTime > startTime) return true;
            // new subtite endTime is within another subtitle
            if (s.startTime < endTime && s.endTime > endTime) return true;
            

            return false;
        });
    }

    validateSlideAndSubslidePosition(article, slidePosition, subslidePosition, changes = {}) {
        slidePosition = parseInt(slidePosition);
        subslidePosition = parseInt(subslidePosition);

        if (!article.slides.find((s) => parseInt(s.position) === slidePosition)) return { valid: false, message: 'Invalid slide index' };
        if (!article.slides.find((s) => parseInt(s.position) === slidePosition).content.find((s) => parseInt(s.position) === subslidePosition)) return { valid: false, message: 'invalid subslide index' };
        const keys = Object.keys(changes);
        let valid = true;
        let message = '';
        let subslideItem = article.slides.find(s => s.position === slidePosition).content.find(s => s.position === subslidePosition);
        // keys.forEach((key) => {
        //     if (key === 'startTime' || key === 'endTime') {
        //         const overlappedSubtitle = this.getOverlappedSubslide({ [key]: changes[key] }, article.slides, [{ slidePosition, subslidePosition }]);
        //         if (overlappedSubtitle) {
        //             if (!(parseInt(overlappedSubtitle.slidePosition) === parseInt(slidePosition) && parseInt(overlappedSubtitle.subslidePosition) === parseInt(subslidePosition))) {
        //                 valid = false;
        //                 message =  `${key} is overlapping with another subtitle`;
        //             }
        //         }
        //         if (key === 'startTime' && subslideItem.endTime < changes[key]) {
        //             valid = false;
        //             message = 'Start time cannot be larger than end time';
        //         }
        //         if (key === 'endTime' && subslideItem.startTime > changes[key]) {
        //             valid = false
        //             message = 'End time cannot be less than start time';
        //         }                
        //     }
        // })
        // console.log( {valid, message });
        return { valid, message };
    }

    validateSubslideUpdate(article, slidePosition, subslidePosition, changes) {
        if (!changes) return { valid: false, message: 'Invalid fields' };
        return this.validateSlideAndSubslidePosition(article, slidePosition, subslidePosition, changes);
    }

    validateSubslideDelete(article, slidePosition, subslidePosition) {
        return this.validateSlideAndSubslidePosition(article, slidePosition, subslidePosition);
    }

    validateSpeakersProfileUpdate(speakersProfile) {
        if (!speakersProfile || !Array.isArray(speakersProfile)) return { valid: false, message: 'speakersProfile must be an array' };
        if (speakersProfile.length > 10) return { valid: false, message: 'Max number of speakers is 10' };
        let hasGender = true;
        speakersProfile.forEach((speaker) => {
            if (!speaker.speakerGender) {
                hasGender = false;
            }
        })
        if (!hasGender) return { valid: false, message: 'Speakers must have a gender' };
        return { valid: true };
    }

    validateTranslatorsUpdate(translators) {
        if (!translators || !Array.isArray(translators)) return { valid: false, message: 'translators must be an array' };
        if (translators.length === 0) return { valid: true };
        if (translators.some((t) => t.speakerNumber === undefined || t.speakerNumber === -1)) return { valid: false, message: 'Invalid format: { speakerNumber: number, user: userId }' }
        return { valid: true };
    }

    validateAddSubslide(article, slidePosition, subslidePosition, startTime, endTime, speakerProfile) {
        // if (!article.slides[slideIndex]) return { valid: false, message: 'Invalid slide index ' };
        if (startTime === undefined || !endTime) return { valid: false, message: 'Invalid start or end time' };
        // Check for new subslide being in the same timespan of another one
        const speakerValid = article.speakersProfile.map((s) => s.speakerNumber).indexOf(speakerProfile.speakerNumber) !== -1 || speakerProfile.speakerNumber === -1;
        if (!speakerValid) return { valid: false, message: 'Invalid speaker number' };
        const { slides } = article;
        startTime = startTime * 1000;
        endTime = endTime * 1000;
        if (slides.length > 0 && slides[0].content.length !== 0) {
            const { valid } = this.validateSlideAndSubslidePosition(article, slidePosition, subslidePosition);
            if (!valid) return this.validateSlideAndSubslidePosition(article, slidePosition, subslidePosition);
        }

        const positionInvalid = slides.reduce((acc, slide, slideIndex) => acc.concat(slide.content.map((s, subslideIndex) => ({ ...s, slideIndex, subslideIndex }))), [])
            .map(this.formatSubslideToSubtitle).find(s => s.startTime === startTime || s.endTime === endTime || (endTime < s.endTime && startTime > s.startTime))
        if (positionInvalid) return { valid: false, message: 'Invalid slide position' };
        return { valid: true };
    }
}



module.exports = new ArticleService();