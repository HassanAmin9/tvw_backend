const { Translate } = require('@google-cloud/translate');
const { googleProjectId } = require('./config');

// Instantiates a client
const translateClient = new Translate({ projectId: googleProjectId });


function translateText(text, targetLang) {
    return new Promise((resolve, reject) => {
        translateClient.translate(text, targetLang)
        .then((res) => {
            resolve(res[0])  
        })
        .catch(reject);
    })
}


module.exports = {
    translateText,
}