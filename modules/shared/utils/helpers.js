module.exports = {
    handlePromiseReject: function (promise) {
        return new Promise((resolve) => {
            promise.then((data) => resolve({ data }))
                .catch(err => resolve({ err }));
        })
    },
    showMoreText: function (text, length = 24) {
        return text.length > length ? `${text.substr(0, length)} ...` : text;
    }

}