const { User } = require('../models/');
const { OrgRole } = require('../models/');

const sha256 = require('sha256');

const BaseHandler = require('./BaseHandler');

class UserHandler extends BaseHandler {
    constructor() {
        super(User);
    }

    async addUser(userDetail) {
        const { permissions } = userDetail;

        let orgRole = OrgRole({
            organization: userDetail.organization,
            organizationOwner: userDetail.organizationOwner ? userDetail.organizationOwner : false,
            permissions
        });

        let newUser = User({
            firstname: userDetail.firstname ? userDetail.firstname : '',
            lastname: userDetail.lastname ? userDetail.lastname : '',
            email: userDetail.email ? userDetail.email : '',
            password: sha256(userDetail.password ? userDetail.password : ''),
            emailVerified: userDetail.isEmailVerified ? userDetail.isEmailVerified : false,
            inviteStatus: userDetail.inviteStatus ? userDetail.inviteStatus : 'accepted',
            organizationRoles: [orgRole]
        });

        const user = await newUser.save();
        return user;
    }

    async updateUser(userID, userDetail) {
        let result = await this.updateOne({ email: userID }, userDetail);
        return result.ok === 1
    }

    async checkUser(email) {
        const userFromDb = await this.findOne({ email: email });

        if (userFromDb !== null) {
            return userFromDb['email'] === email;
        }

        return false;
    }

    async getUserByEmail(email) {
        return await this.findOne({ email: email }).populate('organizationRoles.organization');
    }

    async  getAllUsers() {
        return await this.find();
    }

}

module.exports = new UserHandler();