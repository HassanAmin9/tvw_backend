const articleService = require('../shared/services/article');

const middlewares = {
    authorizeArticleUpdate: function(req, res, next) {
        const { articleId } = req.params;
        articleService.findById(articleId)
        .populate('video')
        .then((article) => {
            article = article.toObject();
            const organizationId = article.organization.toString();
            const organizationRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === organizationId);
            console.log(organizationRole, 'org role')
            if (!organizationRole) return res.status(401).send('Unauthorized');
            if (organizationRole.organizationOwner || organizationRole.permissions.indexOf('admin') !== -1) return next();

            const { reviewers } = article.video;
            console.log('reviewers are', reviewers)
            // If no users are assigned, anyone with review permissions can modify it
            if ((!reviewers || reviewers.length === 0) && organizationRole.permissions.indexOf('review') !== -1) return next();
            // Only assigned reviewers can modify original articles
            if (reviewers.map((r) => r.toString()).indexOf(req.user._id.toString()) !== -1) return next();

            return res.status(401).send("You're not assigned to review this video");
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },
    authorizeArticleAdmin: function(req, res, next) {
        const { articleId } = req.params;
        articleService.findById(articleId)
        .then((article) => {
            const organizationId = article.organization.toString();
            const organizationRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === organizationId);

            if (!organizationRole) return res.status(401).send('Unauthorized');
            if (organizationRole.organizationOwner || organizationRole.permissions.indexOf('admin') !== -1) return next();

            return res.status(401).send('Unauthorized');
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },
    authorizeFinishdateUpdate: function(req, res, next) {
        const { articleId } = req.params;
        const { speakerNumber } = req.body;
        const user = req.user;

        articleService.findById(articleId)
        .then((articleDoc) => {
            if (!articleDoc) throw new Error('invalid article id');
            const translationInvitation = articleDoc.translators.find(t => t.speakerNumber === parseInt(speakerNumber));
            if (!translationInvitation) throw new Error('No one is assigned for this speaker');
            const organizationRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === articleDoc.organization.toString());
            if (!organizationRole) throw new Error('Unauthorized');
            if (organizationRole.organizationOwner || organizationRole.permissions.indexOf('admin') !== -1) return next();
            if (translationInvitation.user.toString() !== user._id.toString()) throw new Error('Unauthorized');
            return next();
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    }
}

module.exports = middlewares;