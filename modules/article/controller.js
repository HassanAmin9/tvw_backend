const uuid = require('uuid').v4;
const async = require('async');
const articleService = require('../shared/services/article');
const storageService = require('../shared/vendors/storage');
const commentService = require('../shared/services/comment');
const videoService = require('../shared/services/video');
const emailService = require('../shared/services/email');
const userService = require('../shared/services/user');
const notificationService = require('../shared/services/notification');
const authService = require('../shared/services/auth');

const { isoLangs } = require('../shared/constants/langs');

const controller = {
    getById: function (req, res) {
        const { articleId } = req.params;
        articleService.findById(articleId)
            .then((article) => {
                if (!article) {
                    return res.send(null);
                }
                // article = articleService.cleanArticleSilentAndBackgroundMusicSlides(article.toObject());
                return res.json({ article: article.toObject() });
            })
            .catch((err) => {
                console.log('error', err);
                return res.status(400).send('Something went wrong');
            })
    },
    deleteArticle: function(req, res) {
        const { articleId } = req.params;
        let article
        articleService.findById(articleId)
        .then((articleDoc) => {
            article = articleDoc.toObject();
            return articleService.remove({ _id: articleId })
        })
        .then(() => {
            // If it's translation article, delete recorded audios
            if (article.articleType === 'translation' && article.slides && article.slides.length > 0) {
                // articleService.deleteArticlesMedia([article]);
            }
            return res.json({ success: true });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    getArticleComments: function(req, res) {
        const { articleId } = req.params;
        const { slidePosition, subslidePosition } = req.query;
        const slidesSubslidesPositions = req.query.slides ? req.query.slides.split(',') : [];
        let article;
        articleService.findById(articleId)
        .then((articleDoc) => {
            if (!articleDoc) throw new Error('Invalid article id');
            article = articleDoc.toObject();
            const commentQuery = {
                article: article._id,
            }
            if (slidesSubslidesPositions.length > 0) {
                const commentsOrQuery = [];
                slidesSubslidesPositions.forEach((sb) => {
                    const [ slidePosition, subslidePosition ] = sb.split('-');
                    commentsOrQuery.push({ slidePosition: parseInt(slidePosition), subslidePosition: parseInt(subslidePosition) });
                })
                commentQuery['$or'] = commentsOrQuery;
            }
            return commentService.find(commentQuery)
                .populate('user', 'email firstname lastname');
        })
        .then((commentsDocs) => {
            const slidesComments = article.slides
                .reduce((acc, s) => acc.concat(s.content.map((sub) => ({ ...sub, slidePosition: s.position }))), [])
                .filter(s => s.speakerProfile && s.speakerProfile.speakerNumber !== -1)
                .map((s, index) => ({ slidePosition: s.slidePosition, subslidePosition: s.position, index, comments: [] }));
            
            commentsDocs.forEach((comment) => {
                const matchingSlide = slidesComments.find((s) => s.slidePosition === comment.slidePosition && s.subslidePosition === s.subslidePosition);
                if (matchingSlide) {
                    matchingSlide.comments.push(comment);
                }
            })
            return res.json({ comments: slidesComments.filter(s => s.comments.length > 0) });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    getByVideoId: function (req, res) {
        console.log('Got request ============================================')
        const { videoId } = req.query;
        videoService.findById(videoId)
        .then((video) => {
            return articleService.findById(video.article)
        })
            .then((article) => {
                if (!article) {
                    return res.send(null);
                }
                return res.json(article);
            })
            .catch((err) => {
                console.log('error', err);
                return res.status(400).send('Something went wrong');
            })
    },

    addSubslide: function (req, res) {
        const { articleId, slidePosition, subslidePosition } = req.params;
        const { text, startTime, endTime, speakerProfile } = req.body;
        articleService.findById(articleId)
            .then((article) => {
                if (!article) return res.status(400).send('Invalid article id');
                const { valid, message } = articleService.validateAddSubslide(article.toObject(), slidePosition, subslidePosition, startTime, endTime, speakerProfile);
                if (!valid) {
                    throw new Error(message)
                }
                return articleService.addSubslide(articleId, slidePosition, subslidePosition, { text, startTime, endTime, speakerProfile });
            })
            .then(() => {
                return articleService.findById(articleId);
            })
            .then((article) => {
                return res.json({ article });
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message || 'Something went wrong');
            })
    },

    updateSubslide: function (req, res) {
        const { articleId, slidePosition, subslidePosition } = req.params;
        const changes = req.body;
        articleService.findById(articleId)
            .then((article) => {
                if (!article) return res.status(400).send('Invalid article');
                const { valid, message } = articleService.validateSubslideUpdate(article.toObject(), slidePosition, subslidePosition, changes);
                console.log('valid ', valid, message);
                if (!valid) {
                    throw new Error(message || 'Something went wrong');
                }
                return articleService.updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, changes);
            })
            .then((changes) => {
                return res.json({ articleId, slidePosition, subslidePosition, changes });
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    },

    splitSubslide: function(req, res) {
        const { articleId, slidePosition, subslidePosition } = req.params;
        const { wordIndex, time } = req.body;
        console.log(req.params, req.body)
        articleService.splitSubslide(articleId, slidePosition, subslidePosition, wordIndex, time)
        .then(() => {
            return articleService.findById(articleId)
        })
        .then((article) => {
            return res.json({ article });
        })
        .catch(err => {
            return res.status(400).send(err.message);
        })
    },

    deleteSubslide: function (req, res) {
        const { articleId, slidePosition, subslidePosition } = req.params;
        const changes = req.body;
        articleService.findById(articleId)
            .then((article) => {
                if (!article) return res.status(400).send('Invalid article');
                const { valid, message } = articleService.validateSubslideDelete(article.toObject(), slidePosition, subslidePosition, changes);
                if (!valid) {
                    throw new Error(message || 'Something went wrong');
                }
                return articleService.removeSubslide(article, slidePosition, subslidePosition);
            })
            .then(() => articleService.findById(articleId))
            .then((article) => {
                return res.json({ article });
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    },

    replaceArticleText: function(req, res) {
        const { articleId } = req.params;
        const { find, replace } = req.body;
        console.log(req.body)
        let article;
        articleService.findById(articleId)
            .then((articleDoc) => {
                article = articleDoc.toObject();

                return articleService.replaceArticleSlidesText(articleId, { find, replace })
            })
            .then((changedSlides) => {
                // changedSlides.forEach(({ slidePosition, subslidePosition, text }) => {
                //     websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(article.organization)).emit(`${events.TRANSLATION_SUBSLIDE_CHANGE}/${articleId}`, { slidePosition, subslidePosition, changes: { text, audioSynced: false } });
                // })
                return articleService.findById(articleId);
            })
            .then((article) => {
                return res.json({ article: articleService.cleanArticleSilentAndBackgroundMusicSlides(article.toObject())})
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send('Something went wrong');
            })
            .then(() => {
                articleService.update({ _id: articleId }, { exported: false })
                .then(() => {

                })
                .catch(err => {
                    console.log('error updating article exported', err);
                })
            })
    },

    updateSpeakersProfile: function (req, res) {
        const { speakersProfile } = req.body;
        const { articleId } = req.params;

        const { valid, message } = articleService.validateSpeakersProfileUpdate(speakersProfile);
        if (!valid) {
            return res.status(400).send(message || 'Something went wrong');
        }
        articleService.update({ _id: articleId }, { speakersProfile })
            .then(() => {
                return videoService.update({ _id: articleId }, { numberOfSpeakers: speakersProfile.length })
            })
            .then(() => {
                return res.json({ speakersProfile })
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send('Something went wrong');
            })
    },

    updateToEnglish: function(req, res) {
        const { articleId } = req.params;
        const { toEnglish } = req.body;
        articleService.update({ _id: articleId }, { toEnglish })
        .then(() => {
            return res.json({ toEnglish });
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },
    
    updateTranslators: function(req, res) {
        const { translators } = req.body;
        const { articleId } = req.params;
        const newTranslators = [];
        const { valid, message } = articleService.validateTranslatorsUpdate(translators);
        if (!valid) {
            return res.status(400).send(message || 'Something went wrong');
        }
        let finalTranslators = [];
        let removedTranslators = [];
        let article;
        articleService.findById(articleId)
        .populate('video')
        .populate('organization')
        .then((articleDoc) => {
            article = articleDoc.toObject();
            
            if (translators.length > 0) {
                // Collect removed translators
                let newTranslatorsIds = translators.map(t => t.user);
                console.log('new ids', newTranslatorsIds, article.translators)
                article.translators.forEach((translator) => {
                    if (newTranslatorsIds.indexOf(translator.user.toString()) === -1 || translators.find(t => t.user === translator.user.toString()).speakerNumber !== translator.speakerNumber) {
                        removedTranslators.push(translator);
                    }
                })
                translators.forEach((translator) => {
                    if (!articleDoc.translators.some((t) => t.speakerNumber === translator.speakerNumber && t.user.toString() === translator.user)) {
                        translator.inviteToken = `${uuid()}-${uuid()}`;
                        finalTranslators.push({
                            user: translator.user,
                            speakerNumber: translator.speakerNumber,
                            inviteToken: translator.inviteToken,
                            invitedBy: req.user._id,
                        })
                        newTranslators.push(translator)
                    } else {
                        finalTranslators.push(translator)
                    }
                })
            } else {
                article.translators.forEach((t) => {
                    removedTranslators.push(t);
                })
            }
            return articleService.update({ _id: articleId }, { translators: finalTranslators })
        })
        .then(() => {
            return articleService.findById(articleId);
        })
        .then((articleDoc) => {
            let translators = articleDoc.toObject().translators.map(t => ({ ...t, inviteToken: '' }));
            return res.json({ translators })
        })
        .then(() => {
            return new Promise((resolve) => {
                // Remove pending invitation notifications from removed translators
                if (removedTranslators.length === 0) return resolve();
                const removeNotiFuncArray = [];
                removedTranslators.forEach((translator) => {
                    removeNotiFuncArray.push((cb) => {

                        const notiQuery = {
                            owner: translator.user,
                            organization: article.organization._id,
                            status: 'pending',
                            resource: articleId 
                        }
                        notificationService.remove(notiQuery)
                        .then(() => {
                            cb();
                        })
                        .catch(err => {
                            console.log('err removing pending notifications', err);
                            cb()
                        })
                    })
                })
                async.parallel(removeNotiFuncArray, () => {
                    resolve();
                })
            })
        })
        .then(() => {
            if (newTranslators.length === 0) return;
            // Send email invitation and notification to new translators
            newTranslators.filter(t => t.user !== req.user._id.toString()).forEach((translator) => {
                const speakerTimingSeconds = article.slides
                                            .reduce((acc, s) => acc.concat(s.content), [])
                                            .filter(s => s.speakerProfile && s.speakerProfile.speakerNumber === translator.speakerNumber)
                                            .reduce((acc, s) => acc + ((s.endTime - s.startTime) || 0), 0)
                const speakerTimingMinutes = parseFloat(speakerTimingSeconds/60).toFixed(1);
                const fromLang = isoLangs[article.video.langCode.split('-')[0]].name;
                const toLang = article.langName || isoLangs[article.langCode.split('-')[0]].name;
                const extraContent = `This video requires you to add voice-overs for "${speakerTimingMinutes}" minutes, taking approximately "${parseFloat(speakerTimingMinutes * 15).toFixed(1)}" minutes of time`;

                let user;
                userService.findById(translator.user)
                .then((userData) => {
                    user = userData.toObject();
                    return authService.generateLoginToken(user.email);

                })
                .then((token) => {
                    const notificationData = {
                        owner: user._id,
                        organization: article.organization._id,
                        from: req.user._id,
                        type: 'invited_to_translate',
                        content: `${req.user.email} has invited you to translate the video "${article.title}" (${article.langCode}) for speaker (${translator.speakerNumber})`,
                        extraContent,
                        resource: articleId,
                        resourceType: 'article',
                        hasStatus: true,
                        status: 'pending',
                        inviteToken: translator.inviteToken,
                    }
                    notificationService.notifyUser({ email: user.email, organization: article.organization._id }, notificationData)
                    .then((doc) => {
                        console.log('created notification', doc)
                    })
                    .catch((err) => {
                        console.log('error creating notification', err);
                    })
                    return emailService.inviteUserToTranslate({
                        from: req.user,
                        to: user,
                        articleId: article._id.toString(),
                        fromLang,
                        toLang,
                        organizationName: article.organization.name,
                        organizationId: article.organization._id,
                        videoTitle: article.video.title,
                        toLangCode: article.langCode || toLang,
                        inviteToken: translator.inviteToken,
                        extraContent,
                    })
                })
                .catch(err => {
                    console.log(err);
                })
            })

        })
        .catch(err => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    },
    
    updateTranslatorFinishDate: function(req, res) {
        const { articleId } = req.params;
        const { speakerNumber, timestamp } = req.body;
        console.log(req.body)
        let article;
        let translators;
        articleService.findById(articleId)
        .then((articleDoc) => {
            if (!articleDoc) throw new Error('Invalid article id');
            article = articleDoc.toObject();
            translators = article.translators;
            translators.find(t => t.speakerNumber === speakerNumber).finishDate = timestamp;
            return articleService.update({ _id: articleId }, { translators });
        })
        .then(() => {
            return res.json({ translators: article.translators });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    getUserTranslations: function(req, res) {
        const perPage = 10;
        let { organization, page, search, user } = req.query;
        const query = {
            articleType: 'translation',
            organization,
        }
        query['translators.user'] = { $eq: user }
        const queryKeys = Object.keys(req.query)
        // Remove page if it's in the query
        if (queryKeys.indexOf('page') !== -1) {
            delete req.query.page
        }
        
        if (queryKeys.indexOf('search') !== -1) {
            query.title = new RegExp(search, 'ig');
            delete req.query.search;
        }

        if (queryKeys.indexOf('archived') !== -1) {
            delete req.query.archived;
        }

        if (page) {
            page = parseInt(page);
        } else {
            page = 1;
        }

        const skip = page === 1 || page === 0 ? 0 : (page * perPage - perPage);

        const metrics = [];
        const articlesWithMetrics = []

        articleService.find(query)
        .populate('video')
        .sort({ created_at: -1 })
        .skip(skip)
        .limit(perPage)
        .then((articles) => {
            articles.forEach((article) => {
                article = articleService.cleanArticleSilentAndBackgroundMusicSlides(article.toObject());
                const { speakersProfile } = article;
                const speakersMetrics = [];
                const subslides = article.slides.slice().reduce((acc, s) => acc.concat(s.content), []);
                speakersProfile.forEach((speaker) => {
                    const totalSpeakerCount = subslides.filter(s => s.speakerProfile.speakerNumber === speaker.speakerNumber).length;
                    const completedAudioCount = subslides.filter((s) => s.text && s.audio && s.speakerProfile.speakerNumber === speaker.speakerNumber).length;
                    speakersMetrics.push({
                        speaker,
                        progress: Math.ceil(completedAudioCount / totalSpeakerCount * 100),
                    })
                });

                const totalSubslidesCount = subslides.length;
                const totalCompletedCount = subslides.filter(s => s.text && s.audio).length;
                const completedAudioCount = subslides.filter(s => s.audio).length
                const completedTextCount = subslides.filter(s => s.text).length;
                
                const audioCompleted = Math.round(completedAudioCount/totalSubslidesCount * 100);
                const textCompleted = Math.round(completedTextCount / totalSubslidesCount * 100);
                const totalCompleted = Math.round(totalCompletedCount / totalSubslidesCount * 100);
                articlesWithMetrics.push({ ...article, metrics: { completed: { audio: audioCompleted, text: textCompleted, total: totalCompleted  }, speakersMetrics }})
                metrics.push(speakersMetrics)
            })
            // Remove slides from the returned object
            articlesWithMetrics.forEach((am) => {
                delete am.slides;
            })

            return articleService.count(query)
        })
        .then((count) => {
            console.log('articles', articlesWithMetrics)
            return res.json({ articles: articlesWithMetrics, pagesCount: Math.ceil(count/perPage) });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    getArticlesTranslations: function(req, res) {
        const perPage = 10;
        let { organization, page, archived, search } = req.query;
        
        if (!archived) {
            archived = false;
        }
        const query = {
            organization,
            status: 'done'
        }
        const queryKeys = Object.keys(req.query)
        // Remove page if it's in the query
        if (queryKeys.indexOf('page') !== -1) {
            delete req.query.page
        }
        
        if (queryKeys.indexOf('search') !== -1) {
            query.title = new RegExp(search, 'ig');
            delete req.query.search;
        }

        if (queryKeys.indexOf('archived') !== -1) {
            delete req.query.archived;
        }

        if (page) {
            page = parseInt(page);
        } else {
            page = 1;
        }

        const skip = page === 1 || page === 0 ? 0 : (page * perPage - perPage);

        Object.keys(req.query).forEach(key => {
            query[key] = req.query[key];
        });
        let videos;
        videoService.find(query)
        .sort({ created_at: -1 })
        .skip(skip)
        .limit(perPage)
        .then(v => {
            videos = v;
            const fetchArticlesFuncArray = [];
            videos.forEach(video => {
                fetchArticlesFuncArray.push((cb) => {
                    const articleQuery = {
                        video: video._id,
                        articleType: 'translation',
                        archived,
                    }
                    articleService.find(articleQuery)
                    .then((articles) => {
                        const metrics = [];
                        const articlesWithMetrics = []
                        articles.forEach((article) => {
                            article = articleService.cleanArticleSilentAndBackgroundMusicSlides(article.toObject());
                            const { speakersProfile } = article;
                            const speakersMetrics = [];
                            const subslides = article.slides.slice().reduce((acc, s) => acc.concat(s.content), []);
                            speakersProfile.forEach((speaker) => {
                                const totalSpeakerCount = subslides.filter(s => s.speakerProfile.speakerNumber === speaker.speakerNumber).length;
                                const completedAudioCount = subslides.filter((s) => s.text && s.audio && s.speakerProfile.speakerNumber === speaker.speakerNumber).length;
                                speakersMetrics.push({
                                    speaker,
                                    progress: Math.ceil(completedAudioCount / totalSpeakerCount * 100),
                                })
                            });

                            const totalSubslidesCount = subslides.length;
                            const totalCompletedCount = subslides.filter(s => s.text && s.audio).length;
                            const completedAudioCount = subslides.filter(s => s.audio).length
                            const completedTextCount = subslides.filter(s => s.text).length;
                            
                            const audioCompleted = Math.round(completedAudioCount/totalSubslidesCount * 100);
                            const textCompleted = Math.round(completedTextCount / totalSubslidesCount * 100);
                            const totalCompleted = Math.round(totalCompletedCount / totalSubslidesCount * 100);
                            articlesWithMetrics.push({ ...article, metrics: { completed: { audio: audioCompleted, text: textCompleted, total: totalCompleted  }, speakersMetrics }})
                            metrics.push(speakersMetrics)
                        })
                        // Remove slides from the returned object
                        articlesWithMetrics.forEach((am) => {
                            delete am.slides;
                        })
                        articleService.findById(video.article)
                        .then((originalArticleDoc) => {
                            const originalArticle = originalArticleDoc.toObject();
                            delete originalArticle.slides;
                            return cb(null, { video, articles: articlesWithMetrics, originalArticle });
                        })
                        .catch(cb);
                    })
                    .catch(err => {
                        console.log(err);
                        return cb();
                    })
                })
            })
            async.parallelLimit(fetchArticlesFuncArray, 10, (err, result) => {
                if (err) throw err;
                videoService.count(query)
                .then((count) => {
                    console.log('count i', count);
                    return res.json({ videos: result, pagesCount: Math.ceil(count/perPage) });
                })
                .catch(err => {
                    console.log(err);
                    return res.json({ videos: result, pagesCount: null});                    
                })
            })
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message || 'Something went wrong');
        })
    }
}

module.exports = controller;