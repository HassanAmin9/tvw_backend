const controller = require('./controller');
const middlewares = require('./middlewares');
// external modules should call the mount function and pass it an instance 
// of the router to add the module's routes to it
const mount = function (router) {
    // Define module routes here

    router.get('/by_article_id/:articleId', controller.getArticleComments);
    router.post('/', controller.addCommet)

    return router;
}

module.exports = {
    mount,
}
