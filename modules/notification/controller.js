const notificationService = require('../shared/services/notification');

const controller = {
    
    getNotifications: function(req, res) {
        const perPage = 10;
        let { page, organization } = req.query;

        if (page) {
            page = parseInt(page);
        } else {
            page = 1;
        }
        const skip = page === 1 || page === 0 ? 0 : (page * perPage - perPage);
        const query = {
            owner: req.user._id,
            organization,
        }
        let count;
        notificationService.count(query)
        .then((c) => {
            count = c || 0;
            return notificationService.find(query)
            .skip(skip)
            .limit(perPage)
            .sort({ created_at: -1 })
            .populate('from', 'firstname lastname email')
        })
        .then((notifications) => {
            return res.json({ notifications, pagesCount: Math.ceil(count/perPage) });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    getUnreadCount: function(req, res) {
        const { organization } = req.query;
        const query = {
            owner: req.user._id,
            organization,
            read: false,
        }
        notificationService.count(query)
        .then((count) => {
            return res.json({ count });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },
    
    setNotificationsRead: function(req, res) {
        const { organization } = req.body;
        const query = {
            owner: req.user._id,
            organization
        }

        notificationService.update(query, { read: true })
        .then(() => {
            return res.json({ success: true });
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    }
   
}


module.exports = controller;