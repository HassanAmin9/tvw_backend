FROM node:10.15.0-jessie
WORKDIR /tvw_backend


# Dont copy nodemodules over
RUN echo "node_modules" >> .dockerignore
# SET PATH FOR gsc_creds.json
RUN echo "GOOGLE_APPLICATION_CREDENTIALS=/tvw_backend/gsc_creds.json" >> .env

ENV GOOGLE_APPLICATION_CREDENTIALS=/tvw_backend/gsc_creds.json

COPY . .
RUN npm install
# ADD AWS CREDENTIALS FILE
ARG AWS_KEYS_FILE_BASE64
RUN mkdir ~/.aws
RUN echo ${AWS_KEYS_FILE_BASE64} | base64 --decode > ~/.aws/credentials

EXPOSE 4000
CMD [ "npm", "run", "docker:prod"]